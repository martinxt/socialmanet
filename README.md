# ConfInfo Android Studio Project

This is the source code of a prototype application implementing the protocol [SocialMANET](dx.doi.org/10.3844/jcssp.2019.1237.1255) revisited to allow MANET mobile station to communicate with stations located in an infrastructure network.


## Installation

Download:

    $ git clone https://martinxt@bitbucket.org/martinxt/social-manet.git

Import Project by Android Studio Menu > File > Import Project...

Click "Sync Project with Gradle Files" and "Rebuild Project" at Android Studio Menu.

Run YourApp by Android Studio Menu > Run > Run YourApp.