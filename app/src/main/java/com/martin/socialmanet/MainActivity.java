package com.martin.socialmanet;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;

import com.google.android.gms.nearby.connection.Payload;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.martin.socialmanet.managers.PublicationManager;
import com.martin.socialmanet.managers.TopicsManager;
import com.martin.socialmanet.model.Publication;
import com.martin.socialmanet.model.Topic;
import com.martin.socialmanet.network.NearbyUtils;
import com.martin.socialmanet.network.NetworkReceiver;
import com.martin.socialmanet.network.RrpConfigHelper;
import com.martin.socialmanet.network.RrpResponse;
import com.martin.socialmanet.network.RrpService;
import com.martin.socialmanet.network.SMConnectionLifecycleCallback;
import com.martin.socialmanet.network.SMEndpointDiscoveryCallback;
import com.martin.socialmanet.network.SMPayloadCallback;
import com.martin.socialmanet.network.utils.ConnectionStatus;
import com.martin.socialmanet.peer.Endpoint;
import com.martin.socialmanet.peer.PeerFragment;
import com.martin.socialmanet.peer.PeerList;
import com.martin.socialmanet.pub.PubFragment;
import com.martin.socialmanet.topic.TopicsFragment;
import com.martin.socialmanet.utils.Constants;
import com.martin.socialmanet.network.utils.MessageType;
import com.martin.socialmanet.utils.PublicationUtils;
import com.martin.socialmanet.utils.Query;
import com.martin.socialmanet.utils.QueryEntry;

import androidx.appcompat.app.AppCompatActivity;
import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.app.TaskStackBuilder;

import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class MainActivity extends AppCompatActivity implements PeerFragment.OnListFragmentInteractionListener {
    private static final String MAIN_ACTIVITY_TAG = "MAIN-ACTIVITY-TAG";
    private static final String NOTIFICATION_CHANEL_ID = "socialmanet-notification-chanel";
    private static int NOTIFICATION_ID = 0;
    public ImageView advertisingIndicator;
    public ImageView discoveringIndicator;

    private static final int PERMISSIONS_REQUEST_CODE_ACCESS_FINE_LOCATION = 1994;
    public static final String FRAGMENT_PEERS_TAG = "fragment-peers";
    public static final String FRAGMENT_TOPICS_TAG = "fragment-topics";
    public static final String FRAGMENT_PUBS_TAG = "fragment-pubs";

    private TopicsManager topicsManager;
    private PublicationManager publicationManager;

    RrpService rrpService;
    // The BroadcastReceiver that tracks network connectivity changes.
    private NetworkReceiver receiver;
    private NearbyUtils nearbyUtils;
    //private TaskExecutor taskExecutor;

    private boolean isAdvertising = false;
    private boolean isDiscovering = false;
    private boolean initializationDone = false;

    public static final int RESEARCH_DELAY_MILLIS = 30 * 1000; //30 seconds
    //private Map<Endpoint, List<QueryEntry>> neighbors = new HashMap<>();
    private Map<String, List<QueryEntry>> neighborsInfos = new HashMap<>();

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSIONS_REQUEST_CODE_ACCESS_FINE_LOCATION:
                if  (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    finish();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == Constants.SETUP_ACTIVITY_REQUEST_CODE) {
            String result = data.getStringExtra(Constants.EXTRA_DONE_ON_SETUP);
            if(result != null && result.equals(Constants.EXTRA_DONE_ON_SETUP_VALUE_CONTINUE)) {
                SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
                boolean committed = prefs.edit()
                        .putBoolean(getString(R.string.pref_app_installed), true)
                        .commit();
                if(committed) {
                    Log.d(MAIN_ACTIVITY_TAG, "Yeah, App installed !!!");
                }
            } else {
                finish();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        advertisingIndicator = findViewById(R.id.advertising_ind);
        discoveringIndicator = findViewById(R.id.discovering_ind);

        createNotificationChannel();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MainActivity.PERMISSIONS_REQUEST_CODE_ACCESS_FINE_LOCATION);
        }

        topicsManager = TopicsManager.getInstance(getApplicationContext());
        publicationManager = PublicationManager.getInstance(getApplicationContext());

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container_view, PubFragment.class, null, FRAGMENT_PUBS_TAG)
                    .setReorderingAllowed(true)
                    .commit();
        }

        // Registers BroadcastReceiver to track network connection changes.
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new NetworkReceiver(isOnline -> handleConnectionStatusChanged(isOnline));
        this.registerReceiver(receiver, filter);

        // Create a RrpService using a Retrofit instance
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RrpConfigHelper.getBaseUrl())
                .addConverterFactory(JacksonConverterFactory.create())
                .build();
        rrpService = retrofit.create(RrpService.class);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Unregisters BroadcastReceiver when app is destroyed.
        if (receiver != null) {
            this.unregisterReceiver(receiver);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(this.initializationDone){
            stopNearbyService();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences prefs = getPreferences(Context.MODE_PRIVATE);
        boolean appInstalled = prefs.getBoolean(getString(R.string.pref_app_installed), false);
        if(!appInstalled) {
            Log.d(MAIN_ACTIVITY_TAG, "App not installed :-(");
            Intent intent=new Intent(MainActivity.this, Setup.class);
            startActivityForResult(intent, Constants.SETUP_ACTIVITY_REQUEST_CODE);
        } else {
            Log.d(MAIN_ACTIVITY_TAG, "App installed :-)");
            initNearbyService();
        }
    }

    @Override
    public void onListFragmentInteraction(Endpoint endpoint) {
        if(endpoint.getEndpointStatus() == ConnectionStatus.FOUND ||
                endpoint.getEndpointStatus() == ConnectionStatus.DISCONNECTED) {
            nearbyUtils.connect(endpoint.getEndpointId(),
                    new SMConnectionLifecycleCallback(this, new SMPayloadCallback(this)));
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_pubs:
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment_container_view, PubFragment.class, null, FRAGMENT_PUBS_TAG)
                            .setReorderingAllowed(true)
                            .commit();
                    return true;
                case R.id.navigation_topics:
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment_container_view, TopicsFragment.class, null, FRAGMENT_TOPICS_TAG)
                            .setReorderingAllowed(true)
                            .commit();
                    return true;
                case R.id.navigation_peers:
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment_container_view, PeerFragment.class, null, FRAGMENT_PEERS_TAG)
                            .setReorderingAllowed(true)
                            .commit();
                    return true;
            }
            return false;
        }
    };

    private void initNearbyService() {
        nearbyUtils = new NearbyUtils(this);
        nearbyUtils.startAdvertising(new SMConnectionLifecycleCallback(this, new SMPayloadCallback(this)));
        nearbyUtils.startDiscovery(new SMEndpointDiscoveryCallback(this));
        //taskExecutor = new TaskExecutor(nearbyUtils, this);
        //taskExecutor.startResearchTask(0, RESEARCH_DELAY_MILLIS, TimeUnit.MILLISECONDS);
        this.initializationDone = true;
    }

    private void stopNearbyService() {
        nearbyUtils.stopAdvertising();
        nearbyUtils.stopDiscovery();
        setAdvertising(false);
        setDiscovering(false);
        //taskExecutor.stop();
    }

    public void handleAdvertisingStatus(boolean succeed) {
        if(succeed) {
            //advertising started
            setAdvertising(true);
        } else {
            //failed to start advertising
            setAdvertising(false);
        }
    }

    public void handleDiscoveringStatus(boolean succeed) {
        if(succeed) {
            //discovering started
            setDiscovering(true);
        } else {
            //failed to start discovering
            setDiscovering(false);
        }
    }

    public void handleRequestConnectionStatus(String endpointId, boolean succeed) {
        if(succeed) {
            // We successfully requested a connection. Now both sides
            // must accept before the connection is established.
            handleConnectionStatus(endpointId, ConnectionStatus.INVITED);
        }
    }

    public void handleEndpointFound(String endpointId) {
        // An endpoint was found. We request a connection to it.
        PeerFragment fragmentPeers = (PeerFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_PEERS_TAG);
        if(fragmentPeers != null) {
            fragmentPeers.getAdapter().add(new Endpoint(endpointId, ConnectionStatus.FOUND));
        }
    }

    public void handleEndpointLost(String endpointId) {
        // A previously discovered endpoint has gone away.
        handleConnectionStatus(endpointId, ConnectionStatus.LOST);
    }

    public void handleConnectionStatus(String endpointId, ConnectionStatus status) {
        Endpoint endpoint;
        int p = PeerList.find(endpointId);
        if(p >= 0) {
            endpoint = PeerList.get(p);
        } else {
            return;
        }
        switch (status) {
            case CONNECTED:
                //device connected
                endpoint.setEndpointStatus(ConnectionStatus.CONNECTED);
                break;
            case INVITED:
                //device invited
                endpoint.setEndpointStatus(ConnectionStatus.INVITED);
                break;
            case DISCONNECTED:
                //device disconnected
                endpoint.setEndpointStatus(ConnectionStatus.DISCONNECTED);
                break;
            case REJECTED:
                //connection rejected by the device
                endpoint.setEndpointStatus(ConnectionStatus.REJECTED);
                break;
            case FOUND:
                //device found
                endpoint.setEndpointStatus(ConnectionStatus.FOUND);
                break;
            case LOST:
                //device lost
                endpoint.setEndpointStatus(ConnectionStatus.LOST);
                break;
            case ERROR:
                //error occur
                endpoint.setEndpointStatus(ConnectionStatus.ERROR);
                break;
            default:
                //unknown status
        }
        if(endpoint.getEndpointStatus() != ConnectionStatus.CONNECTED) {
            neighborsInfos.remove(endpoint.getEndpointId());
        } else {
            // send a QUERY request now.
            String query = computeQuery().getQuery();
            if(!query.isEmpty()) {
                Log.d(MAIN_ACTIVITY_TAG, "QUERY MESSAGE -- " + query);
                nearbyUtils.sendPayload(endpoint.getEndpointId(), nearbyUtils.createPayload(query, MessageType.QUERY));
            }
        }
        PeerFragment fragmentPeers = (PeerFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_PEERS_TAG);
        if(fragmentPeers != null) {
            fragmentPeers.getAdapter().replace(endpoint);
            if(endpoint.getEndpointStatus() == ConnectionStatus.CONNECTED) {
                neighborsInfos.put(endpoint.getEndpointId(), new ArrayList<>());
            }
        }
    }

    public void handleNewPublicationCreated(Publication pub) {
        // dispatch the publication to the RRP if needed
        if(NetworkReceiver.isOnline(getApplicationContext())) {
            dispatchPubsToRRP(Arrays.asList(pub));
        }
        // dispatch the publication created to all the connected neighbors that are interested with.
        List<String> recipients = new ArrayList<>();
        for (Endpoint neighbor:
                neighborsInfos.keySet().stream()
                        .filter(endpointId -> PeerList.find(endpointId) > -1 && PeerList.get(PeerList.find(endpointId)).getEndpointStatus() == ConnectionStatus.CONNECTED)
                        .map(endpointId -> PeerList.get(PeerList.find(endpointId)))
                        .collect(Collectors.toList())) {
            if(neighborsInfos.get(neighbor.getEndpointId()).stream().anyMatch(queryEntry -> queryEntry.getTopicId().equals(pub.getTopicId()))) {
                recipients.add(neighbor.getEndpointId());
            }
        }
        nearbyUtils.sendPayload(recipients, nearbyUtils.createPayload(pub.stringify(), MessageType.PUBLICATION));
    }

    public void handlePayloadReceived(String endpointId, Payload payload) {
        if(payload.getType() == Payload.Type.BYTES) {
            String received = "";
            try {
                received = new String(payload.asBytes(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                // ignore
            }
            int startIndex;
            if(received.startsWith(MessageType.PUBLICATION.toString())) {
                // publication received
                startIndex = MessageType.PUBLICATION.toString().length();
                received = received.substring(startIndex);
                Log.d(MAIN_ACTIVITY_TAG, "PUBLICATIONS RECEIVED --" + received);
                List<Publication> publicationsReceived = PublicationUtils.parseAll(received);
                if(!publicationsReceived.isEmpty()) {
                    publicationManager.saveAll(PublicationUtils.toArray(publicationsReceived));
                    PubFragment fragmentPubs = (PubFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_PUBS_TAG);
                    if(fragmentPubs != null) {
                        fragmentPubs.refresh();
                    }
                    notifyPublicationReceived(publicationsReceived);
                }
            } else if (received.startsWith(MessageType.QUERY.toString())) {
                // QUERY message
                startIndex = MessageType.QUERY.toString().length();
                received = received.substring(startIndex);
                Log.d(MAIN_ACTIVITY_TAG, "QUERY MESSAGE RECEIVED --" + received);
                // Update neighbors
                Query query = Query.makeQuery(received);
                neighborsInfos.put(endpointId, query.getEntries());
                // compute publication to send
                String pubsToSend = PublicationUtils.stringifyAll(computePubsToSend(endpointId));
                if(!pubsToSend.isEmpty()) {
                    Log.d(MAIN_ACTIVITY_TAG, "SENDING PUBLICATIONS--" + pubsToSend);
                    nearbyUtils.sendPayload(endpointId, nearbyUtils.createPayload(pubsToSend, MessageType.PUBLICATION));
                }
                // compute publication to ask
                String pubsToAsk = computePubsToAsk(endpointId);
                if(!pubsToAsk.isEmpty()) {
                    nearbyUtils.sendPayload(endpointId, nearbyUtils.createPayload(pubsToAsk, MessageType.REFRESH_REQUEST));
                }
            } else if(received.startsWith(MessageType.REFRESH_REQUEST.toString())) {
                // handle refresh request
                startIndex = MessageType.REFRESH_REQUEST.toString().length();
                received = received.substring(startIndex);
                Log.d(MAIN_ACTIVITY_TAG, "REFRESH REQUEST RECEIVED --" + received);
                List<Publication> pubs = new ArrayList<>();
                for(String pubId: PublicationUtils.parsePubsToAsk(received)) {
                    pubs.add(publicationManager.getPublication(pubId));
                }
                String pubsToSend = PublicationUtils.stringifyAll(pubs);
                if(!pubsToSend.isEmpty()) {
                    nearbyUtils.sendPayload(endpointId, nearbyUtils.createPayload(pubsToSend, MessageType.PUBLICATION));
                }
            }
        }
    }

    private void notifyPublicationReceived(List<Publication> publications) {
        String title, description;
        if(publications.size() == 0) {
            return;
        }
        else if(publications.size() == 1) {
            Publication publication = publications.get(0);
            if(publication.getTopic() != null){
                title = publication.getTopic().getName();
            } else {
                Topic topic = topicsManager.getTopic(publication.getTopicId());
                if(topic != null) {
                    title = topic.getName();
                } else {
                    title = publication.getTopicId();
                }
            }
            description = publication.getDescription();
        } else {
            title = getString(R.string.pubs_received) + "(" + publications.size() + ")";
            description = publications.get(0).getDescription() + " (...)";
        }
        // Create an Intent for the activity you want to start
        Intent resultIntent = new Intent(this, MainActivity.class);
        // Create the TaskStackBuilder and add the intent, which inflates the back stack
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(resultIntent);
        // Get the PendingIntent containing the entire back stack
        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, NOTIFICATION_CHANEL_ID)
                .setSmallIcon(R.drawable.ic_baseline_announcement_24)
                .setContentTitle(title)
                .setContentText(description)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);;
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(++NOTIFICATION_ID, builder.build());
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void handleConnectionStatusChanged(boolean isOnline) {
        if(isOnline) {
            rrpService.getMissingPublications(computeQuery()).enqueue(new Callback<RrpResponse>() {
                @Override
                public void onResponse(Call<RrpResponse> call, Response<RrpResponse> response) {
                    if(response.isSuccessful()) {
                        // Saving publications fetched from RRP
                        RrpResponse rrpResponse = response.body();
                        List<Publication> fetchedPubs = rrpResponse.getPublications();
                        if(!fetchedPubs.isEmpty()) {
                            publicationManager.saveAll(fetchedPubs.toArray(new Publication[fetchedPubs.size()]));
                            notifyPublicationReceived(fetchedPubs);
                        }

                        // Retrieve and send publications requested by RRP
                        if(!rrpResponse.getPubRequestIds().isEmpty()) {
                            List<Publication> pubsToSend = rrpResponse.getPubRequestIds().parallelStream()
                                    .map(pubId -> publicationManager.getPublication(pubId))
                                    .filter(pub -> pub != null)
                                    .collect(Collectors.toList());
                            if(!pubsToSend.isEmpty()) {
                                dispatchPubsToRRP(pubsToSend);
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<RrpResponse> call, Throwable t) {
                    Log.e(MAIN_ACTIVITY_TAG, "Error while syncing with RRP: " + t.getMessage());
                }
            });
        }
    }

    private void dispatchPubsToRRP(List<Publication> publications) {
        rrpService.savePublications(publications).enqueue(new Callback<List<Publication>>() {
            @Override
            public void onResponse(Call<List<Publication>> call, Response<List<Publication>> response) {
                Log.d(MAIN_ACTIVITY_TAG, response.toString());
                if(response.isSuccessful() && response.body() != null) {
                    Log.d(MAIN_ACTIVITY_TAG, response.body().size() + " publication(s) sent to RRP");
                }
            }

            @Override
            public void onFailure(Call<List<Publication>> call, Throwable t) {
                Log.d(MAIN_ACTIVITY_TAG, "Error while saving pub to RRP: " + t.getMessage());
            }
        });
    }

    private void setAdvertising(boolean advertising) {
        if(this.isAdvertising == advertising) return;
        this.isAdvertising = advertising;
        if(advertising) {
            advertisingIndicator.setImageDrawable(getDrawable(R.drawable.ic_wifi_tethering_green_24dp));
        } else {
            advertisingIndicator.setImageDrawable(getDrawable(R.drawable.ic_wifi_tethering_black_24dp));
        }
    }

    private void setDiscovering(boolean discovering) {
        if(this.isDiscovering == discovering) return;
        this.isDiscovering = discovering;
        if(discovering) {
            discoveringIndicator.setImageDrawable(getDrawable(R.drawable.ic_wifi_green_24dp));
        } else {
            discoveringIndicator.setImageDrawable(getDrawable(R.drawable.ic_wifi_black_24dp));
        }
    }

    private Query computeQuery() {
        Query query = new Query();
        for (Topic topic: topicsManager.getSubscriptions()) {
            query.addEntry(new QueryEntry(topic.getId()));
            for (Publication pub: publicationManager.getPublications(topic)) {
                query.addPublication(topic.getId(), pub.getPublicationId());
            }
        }
        return query;
    }

    private List<Publication> computePubsToSend(String endpointId) {
        List<Publication> publications = new ArrayList<>();
        int position = PeerList.find(endpointId);
        if(position > -1 && neighborsInfos.containsKey(endpointId)) {
            for(Publication localPub: publicationManager.getPublications()) {
                boolean isEligible = neighborsInfos.get(PeerList.get(position).getEndpointId()).stream()
                        .anyMatch(queryEntry -> localPub.getTopicId().equals(queryEntry.getTopicId()) &&
                                queryEntry.getPublicationIdsAsList().stream().noneMatch(id -> id.equals(localPub.getPublicationId())));
                if(isEligible) {
                    publications.add(localPub);
                }
            }
        }
        return publications;
    }

    private String computePubsToAsk(String endpointId) {
        PublicationUtils.resetPubsToAsk();
        int position = PeerList.find(endpointId);
        if(position > -1 && neighborsInfos.containsKey(endpointId)) {
            for(QueryEntry entry: neighborsInfos.get(PeerList.get(position).getEndpointId())) {
                Topic topic = topicsManager.getTopic(entry.getTopicId());
                if(topic != null && topic.isInterested()) {
                    for(String pubId: entry.getPublicationIdsAsList()) {
                        if(publicationManager.getPublication(pubId) == null) {
                            PublicationUtils.addPubToAsk(pubId);
                        }
                    }
                }
            }
        }
        return PublicationUtils.getPubsToAsk();
    }
}
