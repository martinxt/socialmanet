package com.martin.socialmanet.storage;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.martin.socialmanet.model.Publication;

import java.util.List;

import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Single;

@Dao
public interface PublicationDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Publication... publications);

    @Update
    void update(Publication... publications);

    @Delete
    void delete(Publication... publications);

    @Query("SELECT * FROM publication")
    List<Publication> findAll();

    @Query("SELECT * FROM publication WHERE topicId=:tid")
    List<Publication> findAllByTopic(String tid);

    @Query("SELECT * FROM publication WHERE publicationId=:id")
    Publication findById(String id);

}
