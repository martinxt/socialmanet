package com.martin.socialmanet.storage;

import android.content.Context;

import androidx.room.Room;

public class DatabaseHelper {
    private static final String DB_NAME = "social_manet";
    private static AppDatabase db = null;

    public static AppDatabase getInstance(Context ctx) {
        if(db == null) {
            synchronized (AppDatabase.class) {
                db = Room.databaseBuilder(ctx, AppDatabase.class, DB_NAME)
                        .allowMainThreadQueries()
                        .build();
            }
        }
        return db;
    }
}
