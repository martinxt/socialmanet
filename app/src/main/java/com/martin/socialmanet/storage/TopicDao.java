package com.martin.socialmanet.storage;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.martin.socialmanet.model.Topic;

import java.util.List;

@Dao
public interface TopicDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Topic topic);
    
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Topic> topics);

    @Update
    void update(Topic... topics);

    @Delete
    void delete(Topic... topics);

    @Query("SELECT * FROM topic")
    List<Topic> findAll();

    @Query("SELECT * FROM topic WHERE id=:id")
    Topic findById(String id);

    @Query("SELECT * FROM topic WHERE id IN (:ids)")
    List<Topic> findAllByIds(List<String> ids);

    @Query("SELECT * FROM topic WHERE interested")
    List<Topic> findInterested();

    @Query("SELECT * FROM topic WHERE NOT interested")
    List<Topic> findNotInterested();
}
