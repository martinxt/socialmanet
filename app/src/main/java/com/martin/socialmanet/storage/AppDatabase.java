package com.martin.socialmanet.storage;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.martin.socialmanet.model.Publication;
import com.martin.socialmanet.model.Topic;

@Database(entities = {Topic.class, Publication.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public abstract TopicDao topicDao();

    public abstract PublicationDao publicationDao();

}
