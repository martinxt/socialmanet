package com.martin.socialmanet.managers;

import android.content.Context;
import android.util.Log;

import com.martin.socialmanet.R;
import com.martin.socialmanet.model.Topic;
import com.martin.socialmanet.storage.DatabaseHelper;
import com.martin.socialmanet.storage.TopicDao;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Queue;
import java.util.stream.Collectors;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class TopicsManager {
    private static final String TOPIC_MANAGER_TAG = "TOPIC-MANAGER-TAG";
    private static TopicsManager manager = null;
    private static Context appContext;
    private static Locale currentLocale = null;

    private TopicDao topicDao;

    private TopicsManager(Context ctx) {
        this.topicDao = DatabaseHelper.getInstance(ctx).topicDao();
        appContext = ctx;
    }

    public static TopicsManager getInstance(Context ctx) {
        if (manager == null) {
            currentLocale = ctx.getResources().getConfiguration().locale;
            manager = new TopicsManager(ctx);
        }
        return manager;
    }

    public void createTopic(Topic topic) {
        topicDao.insert(topic);
    }

    public void updateTopic(Topic topic) {
        topic.setUpdated(true);
        topic.setCreated(false);
        topic.setDeleted(false);
        topic.setLastUpdateDate(topic.getLastUpdateDate() + 1);
        topicDao.update(topic);
    }

    public void deleteTopic(String topicId) {
        Topic topic = getTopic(topicId);
        if(topic != null) {
            topicDao.delete(topic);
            //topic.setDeleted(true);
            //topic.setCreated(false);
            //topic.setUpdated(false);
            //topicDao.update(topic);
        }
    }

    public List<Topic> getTopics() {
        return topicDao.findAll().stream()
                .filter(t -> !t.isDeleted())
                .collect(Collectors.toList());
    }

    public List<Topic> getSubscriptions() {
        return topicDao.findInterested().stream()
                .filter(t -> !t.isDeleted())
                .collect(Collectors.toList());
    }

    public List<Topic> getNotSubscribedTo() {
        return topicDao.findNotInterested().stream()
                .filter(t -> !t.isDeleted())
                .collect(Collectors.toList());
    }

    public void subscribe(String topicId) {
        Topic topic = topicDao.findById(topicId);
        if (topic != null && !topic.isInterested()) {
            topic.setInterested(true);
            topicDao.update(topic);
        }
    }

    public List<Topic> getTopics(List<String> ids) {
        return topicDao.findAllByIds(ids);
    }

    public Topic getTopic(String topicId) {
        return topicDao.findById(topicId);
    }

    public void unSubscribe(String topicId) {
        Topic topic = topicDao.findById(topicId);
        if (topic != null && topic.isInterested()) {
            topic.setInterested(false);
            topicDao.update(topic);
        }
    }

    /**
     * Load the default topics from file an insert them to the database.
     * To be called once during the app setup
     */
    public void initialize() {
        List<Topic> topics = loadTopics();
        Log.d(TOPIC_MANAGER_TAG, topics.size() + " topics loaded");
        topicDao.insertAll(topics);
    }

    /**
     * load default topics from file
     * @return topicList
     */
    private List<Topic> loadTopics() {
        List<Topic> topicList = new ArrayList<>();
        InputStream inputStream;
        if(currentLocale.getLanguage().equals(new Locale("en").getLanguage())) {
            inputStream = appContext.getResources().openRawResource(R.raw.subjects_en);
        }
        else if(currentLocale.getLanguage().equals(new Locale("fr").getLanguage())) {
            inputStream = appContext.getResources().openRawResource(R.raw.subjects_fr);
        }
        else {
            inputStream = appContext.getResources().openRawResource(R.raw.subjects);
        }

        DocumentBuilder builder = null;
        try {
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = builder.parse(inputStream, null);
            final String rootId = appContext.getResources().getString(R.string.subject_root_element_id);
            Element root = doc.getElementById(rootId);

            NodeList subjectElements = root.getChildNodes();
            // Loading subject appearing in level 1
            for(int i = 0; i < subjectElements.getLength(); i++) {
                if(subjectElements.item(i) instanceof Element) {
                    Element topicElement = (Element) subjectElements.item(i);
                    String code = topicElement.getAttribute("code");
                    String name = topicElement.getAttribute("name");
                    String description = topicElement.getAttribute("description");
                    // the id of a subject of level 1 is its code
                    Topic baseTopic = new Topic(code, code, name, description);
                    List<Topic> subTopics = this.loadChildren(topicElement, baseTopic.getId());
                    baseTopic.setDimension(subTopics.size());
                    topicList.add(baseTopic);
                    topicList.addAll(subTopics);
                }
            }
        } catch (ParserConfigurationException e) {
            //ignore
        } catch (SAXException e) {
            //ignore
        } catch (IOException e) {
            //ignore
        }

        return topicList;
    }

    /**
     * Load the children of a subject represented by -element-, whose id is
     * -fatherId- as an ArrayList of Subject
     * @param element
     * @param fatherId
     * @return children
     */
    private ArrayList<Topic> loadChildren(Element element, String fatherId) {
        ArrayList<Topic> children = new ArrayList<>();
        NodeList childrenElements = element.getChildNodes();
        for(int i = 0; i < childrenElements.getLength(); i++) {
            if(childrenElements.item(i) instanceof Element) {
                Element childElement = (Element) childrenElements.item(i);
                String code = childElement.getAttribute("code");
                String name = childElement.getAttribute("name");
                String description = childElement.getAttribute("description");
                /**
                 * the id of a child subject is the one of its father, follow
                 * by its code, both separated by a dot (.)
                 */
                String id = fatherId + "." + code;
                Topic childTopic = new Topic(id, code, name, description);
                List<Topic> childSubtopics = this.loadChildren(childElement, childTopic.getId());
                childTopic.setDimension(childSubtopics.size());
                children.add(childTopic);
                children.addAll(childSubtopics);
            }
        }
        return children;
    }
}
