package com.martin.socialmanet.managers;

import android.content.Context;

import com.martin.socialmanet.model.Publication;
import com.martin.socialmanet.model.Topic;
import com.martin.socialmanet.storage.DatabaseHelper;
import com.martin.socialmanet.storage.PublicationDao;
import com.martin.socialmanet.utils.Query;
import com.martin.socialmanet.utils.QueryEntry;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import io.reactivex.rxjava3.functions.Consumer;

public class PublicationManager {
    private static Context appContext;
    private PublicationDao publicationDao = null;
    private static PublicationManager manager;

    private PublicationManager(Context ctx) {
        this.appContext = ctx;
        this.publicationDao = DatabaseHelper.getInstance(ctx).publicationDao();
    }

    public static PublicationManager getInstance(Context ctx) {
        if (manager == null) {
            manager = new PublicationManager(ctx);
        }
        return manager;
    }

    public List<Publication> getPublications() {
        List<Publication> publications = publicationDao.findAll();
        TopicsManager topicManager = TopicsManager.getInstance(this.appContext);
        List<String> topicIds = publications.stream().map(p -> p.getTopicId()).collect(Collectors.toList());
        List<Topic> topics = topicManager.getTopics(topicIds);
        List<Publication> results = new ArrayList<>();
        for(Publication pub: publications) {
            pub.setTopic(topics.stream().filter(t -> t.getId().equals(pub.getTopicId())).findFirst().orElse(null));
            results.add(pub);
        }
        return results;
    }

    public List<Publication> getPublications(Topic topic) {
        List<Publication> publications = publicationDao.findAllByTopic(topic.getId());
        List<Publication> results = new ArrayList<>();
        for(Publication pub: publications) {
            pub.setTopic(topic);
            results.add(pub);
        }
        return results;
    }

    public Publication getPublication(String pubId) {
        Publication publication = publicationDao.findById(pubId);
        if(publication != null) {
            publication.setTopic(topicOf(publication));
        }
        return publication;
    }

    public void savePublication(Publication pub) {
        publicationDao.insert(pub);
    }

    public void saveAll(Publication... pubs) {
        publicationDao.insert(pubs);
    }

    public void deletePublication(String pubId) {
        Publication publication = publicationDao.findById(pubId);
        if(publication != null) {
            publicationDao.delete(publication);
        }
    }

    public Topic topicOf(Publication pub) {
        TopicsManager topicManager = TopicsManager.getInstance(this.appContext);
        return topicManager.getTopic(pub.getTopicId());
    }

    public void computeQuery(Consumer<? extends Query> onSuccess) {
       Query query = new Query();
        QueryEntry queryEntry;
        for(Publication publication: getPublications()) {
            if(publication.isValid()) {
                queryEntry = new QueryEntry(publication.getTopicId(), publication.getPublicationId());
                if(!query.addEntry(queryEntry)) {
                    query.addPublication(publication.getTopicId(), publication.getPublicationId());
                }
            }
        }
    }

}
