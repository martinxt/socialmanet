package com.martin.socialmanet.utils;

import java.util.Date;

public class DateUtils {
    public static Date dateFromInt(int millis) {
        Date date = null;
        try {
            date = new Date(millis);
        } catch (Exception e) {
            // ignore
        }
        return date;
    }
}
