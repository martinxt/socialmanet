package com.martin.socialmanet.utils;

import java.util.ArrayList;
import java.util.Iterator;

public class Query {
    private ArrayList<QueryEntry> entries;

    public String getQuery() {
        String query = "";
        for (QueryEntry q: entries) {
            String newQuery = q.getTopicId() + ">" + q.getPublicationIds();
            if (query.equals("")) query = newQuery;
            else query = query + "~" + newQuery;
        }
        return query;
    }

    public Query(){
        entries = new ArrayList<>();
    }

    public boolean addEntry(QueryEntry e){
        boolean exist = false;
        Iterator<QueryEntry> iterator = entries.iterator();
        while (!exist && iterator.hasNext()){
            QueryEntry entry = iterator.next();
            if (entry.getTopicId() == e.getTopicId()) exist = true;
        }
        if (!exist){
            entries.add(e);
            return true;
        }
        return false;
    }

    public void addPublication(String topicId, String publicationId){
        for (QueryEntry queryEntry : getEntries()) {
            if(queryEntry.getTopicId() == topicId){
                queryEntry.addPublicationId(publicationId);
            }
        }
    }

    public static Query makeQuery(String text){
        Query q = new Query();
        for(String entry : text.split("~")){
            if (!entry.equals("")){
                String[] parts = entry.split(">");
                String topic = parts[0];
                String events = "";
                if (parts.length > 1) {
                    events = parts[1];
                }
                q.addEntry(new QueryEntry(topic, events));
            }
        }
        return q;
    }

    public ArrayList<QueryEntry> getEntries(){
        return entries;
    }
}
