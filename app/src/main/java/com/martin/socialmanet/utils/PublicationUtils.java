package com.martin.socialmanet.utils;

import com.martin.socialmanet.model.Publication;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PublicationUtils {
    public static final String SEPARATOR = "_.._";
    private static List<String> PubsToAsk = new ArrayList<>();

    public static String stringifyAll(List<Publication> pubs) {
        String toReturn = "";
        if(pubs.size() > 0) {
            toReturn = pubs.get(0).stringify();
        }
        for (int i = 1; i<pubs.size(); i++) {
            toReturn += SEPARATOR + pubs.get(0).stringify();
        }
        return toReturn;
    }

    public static List<Publication> parseAll(String str) {
        List<Publication> toReturn = new ArrayList<>();
        String[] pubs = str.split(SEPARATOR);
        for (int i = 0; i<pubs.length; i++) {
            toReturn.add(Publication.parse(pubs[i]));
        }
        return toReturn;
    }

    public static Publication[] toArray(List<Publication> publications) {
        Publication[] toReturn = new Publication[publications.size()];
        for (int i=0; i<publications.size(); i++) {
            toReturn[i] = publications.get(i);
        }
        return toReturn;
    }

    public static void resetPubsToAsk() {
        PubsToAsk.clear();
    }

    public static void addPubToAsk(String pubId) {
        PubsToAsk.add(pubId);
    }

    public static String getPubsToAsk() {
        String res = "";
        for (String pubId: PubsToAsk) {
            if(res.isEmpty()) {
                res = pubId;
            } else {
                res = res + SEPARATOR + pubId;
            }
        }
        return res;
    }

    public static List<String> parsePubsToAsk(String pta) {
        String[] items = pta.split(SEPARATOR);
        List<String> res = new ArrayList<>();
        for (String it: items) {
            res.add(it);
        }
        return res;
    }
}
