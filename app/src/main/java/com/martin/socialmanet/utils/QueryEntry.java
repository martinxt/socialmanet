package com.martin.socialmanet.utils;

import java.util.ArrayList;
import java.util.List;

public class QueryEntry {
    private String topicId;
    private String publicationIds;

    public QueryEntry(String topicId) {
        this.topicId = topicId;
        this.publicationIds = "";
    }
    public QueryEntry(String topicId, String publicationId) {
        this.topicId = topicId;
        this.publicationIds = publicationId;
    }

    public void addPublicationId(String e){
        if (!publicationIds.contains(e)){
            if (publicationIds.equals("")) {
                publicationIds = e;
            } else {
                publicationIds = publicationIds + SEPARATOR() + e;
            }
        }
    }

    public List<String> getPublicationIdsAsList(){
        List<String> list = new ArrayList<>();
        for (String s: publicationIds.split(SEPARATOR())){
            if (!s.equals("")) list.add(s);
        }
        return list;
    }

    private static final String SEPARATOR() {
        return "_._.";
    }

    public String getPublicationIds(){
        return publicationIds;
    }

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }
}
