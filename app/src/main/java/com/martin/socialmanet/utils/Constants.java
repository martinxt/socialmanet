package com.martin.socialmanet.utils;

public class Constants {
    public static final int SETUP_ACTIVITY_REQUEST_CODE = 1994;
    public static final String EXTRA_DONE_ON_SETUP = "DONE_ON_SETUP";
    public static final String EXTRA_DONE_ON_SETUP_VALUE_CONTINUE = "CONTINUE";
    public static final String EXTRA_DONE_ON_SETUP_VALUE_EXIT = "EXIT";
    public static String fileContentHead =
        "<!DOCTYPE html>\n" +
        "<html>\n" +
        "    <head>\n" +
        "        <meta charset=\"utf-8\"/>" +
        "        <title>Message</title>\n" +
        "        <style type=\"text/css\">\n" +
        "            body {\n" +
        "                width: 100%;" +
        "                font-family: 'Courier New', Courier, monospace;\n" +
        "                font-size: 14px;\n" +
        "                text-align: justify;\n" +
        "            }\n" +
        "        </style>\n" +
        "    </head>\n" +
        "    <body>\n" +
        "        <h4>Message</h4>\n" +
        "        <p>";

    public static String fileContentFoot =
        "        </p>\n" +
        "    </body>\n" +
        "</html>";
}
