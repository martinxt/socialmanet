package com.martin.socialmanet.peer;

import java.util.ArrayList;
import java.util.List;

public class PeerList {
    private static List<Endpoint> endpointList= new ArrayList<>();

    public static int add(Endpoint endpoint) {
        if(find(endpoint.getEndpointId()) < 0) {
            endpointList.add(endpoint);
            return PeerList.size() - 1;
        } else {
            return -1;
        }
    }

    public static void clear() {
        endpointList.clear();
    }

    public static Endpoint get(int position) {
        return endpointList.get(position);
    }

    public static int size() {
        return endpointList.size();
    }

    public static int find(String endpointId) {
        for (int i = 0; i < endpointList.size(); i++) {
            if(get(i).getEndpointId().equals(endpointId)) {
                return i;
            }
        }
        return -1;
    }

    public static int findAndReplace(Endpoint endpoint) {
        int position = find(endpoint.getEndpointId());
        if(position >= 0) {
            endpointList.set(position, endpoint);
        }
        return position;
    }

    public static List<Endpoint> getEndpointList() {
        return endpointList;
    }

}
