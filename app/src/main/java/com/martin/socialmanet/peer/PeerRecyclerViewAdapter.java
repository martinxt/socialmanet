package com.martin.socialmanet.peer;

import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.martin.socialmanet.R;
import com.martin.socialmanet.network.utils.ConnectionStatus;
import com.martin.socialmanet.peer.PeerFragment.OnListFragmentInteractionListener;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link android.net.wifi.p2p.WifiP2pDevice} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 */
public class PeerRecyclerViewAdapter extends RecyclerView.Adapter<PeerRecyclerViewAdapter.ViewHolder> {

    private final OnListFragmentInteractionListener mListener;

    public PeerRecyclerViewAdapter(OnListFragmentInteractionListener listener) {
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_peer, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = PeerList.get(position);
        holder.deviceName.setText(PeerList.get(position).getEndpointId());
        holder.deviceStatus.setText(getDeviceStatus(PeerList.get(position).getEndpointStatus(), holder.mView.getContext()));
        holder.deviceStatusIndicator.setImageDrawable(getDeviceStatusIndicator(PeerList.get(position).getEndpointStatus(), holder.mView.getContext()));
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return PeerList.size();
    }

    public void add(Endpoint endpoint) {
        int pos = PeerList.add(endpoint);
        if(pos >= 0) {
            notifyItemInserted(pos);
        }
    }

    public void replace(Endpoint endpoint) {
        int position = PeerList.findAndReplace(endpoint);
        if(position >= 0) {
            notifyItemChanged(position);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public Endpoint mItem;

        public final TextView deviceName;
        public final TextView deviceStatus;
        public final ImageView deviceStatusIndicator;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            deviceName = view.findViewById(R.id.peer_device_name);
            deviceStatus = view.findViewById(R.id.peer_device_status);
            deviceStatusIndicator = view.findViewById(R.id.peer_availability_indicator);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + deviceName.getText() + "'";
        }
    }

    private String getDeviceStatus(ConnectionStatus status, Context ctx) {
        switch (status) {
            case CONNECTED:
                return ctx.getString(R.string.connected);
            case INVITED:
                return ctx.getString(R.string.invited);
            case FOUND:
                return ctx.getString(R.string.found);
            case DISCONNECTED:
                return ctx.getString(R.string.disconnected);
            case ERROR:
                return ctx.getString(R.string.failed);
            case REJECTED:
                return ctx.getString(R.string.failed);
            case LOST:
                return ctx.getString(R.string.unavailable);
            default:
                return ctx.getString(R.string.unknown);
        }
    }

    private Drawable getDeviceStatusIndicator(ConnectionStatus status, Context ctx) {
        switch (status) {
            case CONNECTED:
                return ctx.getDrawable(R.drawable.ic_done_all_green_24dp);
            case INVITED:
                return ctx.getDrawable(R.drawable.ic_done_all_black_24dp);
            case FOUND:
                return ctx.getDrawable(R.drawable.ic_done_black_24dp);
            case DISCONNECTED:
                return ctx.getDrawable(R.drawable.ic_done_black_24dp);
            case REJECTED:
                return ctx.getDrawable(R.drawable.ic_radio_button_unchecked_black_24dp);
            case LOST:
                return ctx.getDrawable(R.drawable.ic_radio_button_unchecked_black_24dp);
            case ERROR:
                return ctx.getDrawable(R.drawable.ic_error_outline_red_24dp);
            default:
                return ctx.getDrawable(R.drawable.ic_error_outline_red_24dp);
        }
    }
}
