package com.martin.socialmanet.peer;

import com.martin.socialmanet.model.Publication;
import com.martin.socialmanet.network.utils.ConnectionStatus;

import java.util.ArrayList;

public class Endpoint {
    private String endpointId;
    private ConnectionStatus endpointStatus;
    private long storedTime;
    private ArrayList<Publication> events;

    public Endpoint(String endpointId, ConnectionStatus endpointStatus) {
        this.endpointId = endpointId;
        this.endpointStatus = endpointStatus;
    }

    public String getEndpointId() {
        return endpointId;
    }

    public ConnectionStatus getEndpointStatus() {
        return endpointStatus;
    }

    public void setEndpointStatus(ConnectionStatus endpointStatus) {
        this.endpointStatus = endpointStatus;
    }

    public long getStoredTime() {
        return storedTime;
    }

    public void setStoredTime(long storedTime) {
        this.storedTime = storedTime;
    }

    public ArrayList<Publication> getEvents() {
        return events;
    }

    public void setEvents(ArrayList<Publication> events) {
        this.events = events;
    }
}
