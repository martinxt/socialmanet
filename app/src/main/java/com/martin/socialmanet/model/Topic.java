package com.martin.socialmanet.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.ArrayList;
import java.util.Iterator;

@Entity
public class Topic {
    @NonNull
    @PrimaryKey
    private String id;

    private String code;
    private String name;
    private String description;
    private int dimension;
    private boolean created;
    private boolean updated;
    private boolean deleted;
    private int lastUpdateDate;
    private boolean interested;
    private boolean creator;

    public Topic(String id, String code, String name, String description, int dimension, boolean created, boolean updated, boolean deleted, int lastUpdateDate) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.description = description;
        this.dimension = dimension;
        this.created = created;
        this.updated = updated;
        this.deleted = deleted;
        this.lastUpdateDate = lastUpdateDate;
        this.interested = false;
    }

    @Ignore
    public Topic(String id) {
        this.id = id;
        this.dimension = 0;
        this.created = false;
        this.updated = false;
        this.deleted = false;
        this.lastUpdateDate = 0;
        this.interested = false;
    }

    @Ignore
    public Topic(String id, String code, String name, String description) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.description = description;
        this.dimension = 0;
        this.created = false;
        this.updated = false;
        this.deleted = false;
        this.lastUpdateDate = 0;
        this.interested = false;
    }

    public boolean isFatherOf(Topic t) {
        return t.isSonOf(this);
    }

    public boolean isSonOf(Topic t) {
        return this.id.startsWith(t.getId());
    }

    public Topic clone() {
        Topic t = new Topic(this.id, this.code, this.name, this.description, this.dimension, this.created, this.updated, this.deleted, this.lastUpdateDate);
        t.interested = this.interested;
        return t;
    }

    public boolean equals(Topic t) {
        if(!this.id.equals(t.getId())) return false;
        return true;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDimension() {
        return dimension;
    }

    public void setDimension(int dimension) {
        this.dimension = dimension;
    }

    public boolean isCreated() {
        return created;
    }

    public void setCreated(boolean created) {
        this.created = created;
    }

    public boolean isUpdated() {
        return updated;
    }

    public void setUpdated(boolean updated) {
        this.updated = updated;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public int getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(int lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public boolean isInterested() {
        return interested;
    }

    public void setInterested(boolean interested) {
        this.interested = interested;
    }

    public boolean isCreator() {
        return creator;
    }

    public void setCreator(boolean creator) {
        this.creator = creator;
    }
}
