package com.martin.socialmanet.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.martin.socialmanet.peer.Endpoint;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.UUID;

@Entity
public class Publication {
    @NonNull
    @PrimaryKey
    private String publicationId;

    private String topicId;
    private long publishDate;
    private long validity;
    private String description;
    private String path;

    //not persisted fields
    @Ignore
    private String fileName;
    @Ignore
    private Topic topic;
    @Ignore
    private ArrayList<Endpoint> receivers;

    @Ignore
    public final static long DEFAULT_PUBLICATION_VALIDITY = 1000 * 60 * 60 * 24 * 30; //30 days

    public Publication(String publicationId) {
        this.publicationId = publicationId;

        receivers = new ArrayList<>();
    }

    /**
     *
     * @param publicationId
     * @param topicId
     * @param publishDate
     * @param validity the date when this publication expires
     * @param description
     */
    @Ignore
    public Publication(String publicationId,
                 String topicId,
                 long publishDate,
                 long validity,
                 String description,
                 String path) {
        this.publicationId = publicationId;
        this.topicId = topicId;
        this.publishDate = publishDate;
        this.validity = validity;
        this.description = description;
        this.path = path;

        receivers = new ArrayList<>();
    }

    @Ignore
    public Publication(String publicationId, String topicId, long publishDate, long validity, String description) {
        this.publicationId = publicationId;
        this.topicId = topicId;
        this.publishDate = publishDate;
        this.validity = validity;
        this.description = description;

        receivers = new ArrayList<>();
    }

    @Ignore
    public Publication(String publicationId, String topicId) {
        this.publicationId = publicationId;
        this.topicId = topicId;

        receivers = new ArrayList<>();
    }

    @Ignore
    public Publication(Topic topic) {
        this.topicId = topic.getId();
        this.publicationId = UUID.randomUUID().toString();
        this.publishDate = new Date().getTime();
        this.validity = this.publishDate + DEFAULT_PUBLICATION_VALIDITY;

        this.topic = topic;
        this.receivers = new ArrayList<>();
    }

    @Ignore
    public Publication() {
        this.publicationId = UUID.randomUUID().toString();
        receivers = new ArrayList<>();
    }

    public void addReceiver(Endpoint n) {
        if (!isAReceiver(n)) {
            receivers.add(n);
        }
    }

    public void removeReceiver(Endpoint n) {
        Endpoint neighbor = findReceiver(n.getEndpointId());
        if (neighbor != null) {
            receivers.remove(neighbor);
        }
    }

    public boolean isAReceiver(Endpoint neighbor) {
        return findReceiver(neighbor.getEndpointId()) != null;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Endpoint findReceiver(String neighborId) {
        Iterator<Endpoint> i = receivers.iterator();
        Endpoint n;
        while (i.hasNext()) {
            n = i.next();
            if (n.getEndpointId().equals(neighborId)) {
                return n;
            }
        }
        return null;
    }

    public ArrayList<Endpoint> getReceivers() {
        return receivers;
    }

    public String stringify(){
        String toReturn = topicId+"@"+publicationId+"@"+publishDate+"@"+validity+"@"+description;
        /**
         * File f = new File(getPath());
         *         String filename;
         *         if(f.isFile()) {
         *             filename = f.getName();
         *             toReturn += "@"+filename;
         *         }
         */
        return toReturn;
    }

    public static Publication parse(String e){
        String[] champs = e.split("@");
        Publication res = new Publication();
        res.setTopicId(champs[0]);
        res.setPublicationId(champs[1]);
        res.setPublishDate(Long.parseLong(champs[2]));
        res.setValidity(Long.parseLong(champs[3]));
        res.setDescription(champs[4]);
        if(champs.length >= 6) {
            res.setFileName(champs[5]);
        }
        return res;
    }

    public String getPublicationId() {
        return publicationId;
    }

    public void setPublicationId(String publicationId) {
        this.publicationId = publicationId;
    }

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public long getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(long publishDate) {
        this.publishDate = publishDate;
    }

    public long getValidity() {
        return validity;
    }

    public void setValidity(long validity) {
        this.validity = validity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public void setReceivers(ArrayList<Endpoint> receivers) {
        this.receivers = receivers;
    }

    public boolean isValid() {
        return this.getValidity() > Calendar.getInstance().getTimeInMillis();
    }
}
