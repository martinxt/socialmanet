package com.martin.socialmanet;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.martin.socialmanet.managers.TopicsManager;
import com.martin.socialmanet.utils.Constants;

public class Setup extends AppCompatActivity {
    private static final String SETUP_ACTIVITY_TAG = "SETUP-ACTIVITY-TAG";
    Button initialise, quit;
    boolean pending = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);

        initialise = findViewById(R.id.st_btn_done);
        quit = findViewById(R.id.st_btn_quit);

        initialise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pending) {
                    return;
                }
                pending = true;
                Toast.makeText(Setup.this, getText(R.string.wait), Toast.LENGTH_SHORT).show();

                TopicsManager manager = TopicsManager.getInstance(getApplicationContext());
                Log.d(SETUP_ACTIVITY_TAG, "Starting initialisation. pending = " + pending);
                manager.initialize();
                Log.d(SETUP_ACTIVITY_TAG, "Initialisation done!");

                Intent intent=new Intent();
                intent.putExtra(Constants.EXTRA_DONE_ON_SETUP, Constants.EXTRA_DONE_ON_SETUP_VALUE_CONTINUE);
                setResult(Constants.SETUP_ACTIVITY_REQUEST_CODE, intent);
                finish();
            }
        });

        quit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pending) {
                    return;
                }
                Intent intent = new Intent(Setup.this, MainActivity.class);
                intent.putExtra(Constants.EXTRA_DONE_ON_SETUP, Constants.EXTRA_DONE_ON_SETUP_VALUE_EXIT);
                setResult(Constants.SETUP_ACTIVITY_REQUEST_CODE, intent);
                finish();
            }
        });
    }
}
