package com.martin.socialmanet.topic;

import androidx.recyclerview.widget.RecyclerView;

import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.martin.socialmanet.R;
import com.martin.socialmanet.model.Topic;

import java.util.List;

public class TopicRecyclerViewAdapter extends RecyclerView.Adapter<TopicRecyclerViewAdapter.ViewHolder> {
    private List<Topic> topicList;
    private final TopicListFragment mListener;

    public TopicRecyclerViewAdapter(List<Topic> topics, TopicListFragment listener) {
        topicList = topics;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_topic, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = this.topicList.get(position);
        String topicName = this.topicList.get(position).getName();
        holder.mTopicAvatarText.setText(""+topicName.charAt(0));
        holder.mTopicName.setText(topicName);
        holder.mTopicDesc.setText(this.topicList.get(position).getDescription());
    }

    @Override
    public int getItemCount() {
        return this.topicList.size();
    }

    public void setTopicList(List<Topic> topicList) {
        this.topicList = topicList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements
            View.OnCreateContextMenuListener, MenuItem.OnMenuItemClickListener{
        public final View mView;
        public final ImageView mTopicAvatar;
        public final TextView mTopicName;
        public final TextView mTopicDesc;
        public final TextView mTopicAvatarText;
        public Topic mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mTopicAvatar = view.findViewById(R.id.it_topic_avatar);
            mTopicName = view.findViewById(R.id.it_topic_name);
            mTopicDesc = view.findViewById(R.id.it_topic_desc);
            mTopicAvatarText = view.findViewById(R.id.it_topic_avatar_text);
            view.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            menu.add(Menu.NONE, R.id.menu_topic_action_new, Menu.NONE, R.string.new_topic)
                    .setOnMenuItemClickListener(this);
            if(mItem.isInterested()) {
                menu.add(Menu.NONE, R.id.menu_topic_action_unsubscribe, Menu.NONE, R.string.topic_unsubscribe)
                        .setOnMenuItemClickListener(this);
            } else {
                menu.add(Menu.NONE, R.id.menu_topic_action_subscribe, Menu.NONE, R.string.topic_subscribe)
                        .setOnMenuItemClickListener(this);
            }
            if(mItem.isCreator()) {
                menu.add(Menu.NONE, R.id.menu_topic_action_edit, Menu.NONE, R.string.topic_edit)
                        .setOnMenuItemClickListener(this);
                menu.add(Menu.NONE, R.id.menu_topic_action_delete, Menu.NONE, R.string.topic_delete)
                        .setOnMenuItemClickListener(this);
            }
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            mListener.onMenuItemSelected(item, mItem);
            return false;
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTopicName.getText() + "'";
        }
    }

}
