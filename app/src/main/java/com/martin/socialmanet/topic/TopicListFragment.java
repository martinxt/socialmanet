package com.martin.socialmanet.topic;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.martin.socialmanet.R;
import com.martin.socialmanet.managers.TopicsManager;
import com.martin.socialmanet.model.Topic;

import java.util.ArrayList;
import java.util.List;

public class TopicListFragment extends Fragment {

    public static final String ARG_FRAGMENT_SUBSCRIPTIONS = "fragment-subscriptions";
    public static final String ARG_FRAGMENT_TOPICS = "fragment-topics";
    public static final String TAG_FRAGMENT_TYPE = "tag-fragment-type";

    private String fragmentType = "";
    private TopicRecyclerViewAdapter adapter;
    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public TopicListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            fragmentType = getArguments().getString(TAG_FRAGMENT_TYPE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_topic_list, container, false);
        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));

            TopicsManager topicsManager = TopicsManager.getInstance(getContext());
            List<Topic> topicList;
            if(fragmentType.equals(ARG_FRAGMENT_SUBSCRIPTIONS)) {
                topicList = topicsManager.getSubscriptions();
            } else {
                topicList = topicsManager.getNotSubscribedTo();
            }
            adapter = new TopicRecyclerViewAdapter(topicList, this);
            recyclerView.setAdapter(adapter);
            registerForContextMenu(recyclerView);
        }
        return view;
    }

    public void onMenuItemSelected(MenuItem item, Topic topic) {
        TopicsManager topicsManager = TopicsManager.getInstance(getContext());
        TopicsFragment parentFragment;
        String itemDesc = topic == null? "" : topic.getName();
        TopicEditFragment topicEditFragment;
        switch (item.getItemId()) {
            case R.id.menu_topic_action_subscribe:
                topicsManager.subscribe(topic.getId());
                parentFragment = (TopicsFragment) getParentFragment();
                if(parentFragment != null) {
                    parentFragment.refreshAllFragments();
                }
                break;
            case R.id.menu_topic_action_unsubscribe:
                topicsManager.unSubscribe(topic.getId());
                parentFragment = (TopicsFragment) getParentFragment();
                if(parentFragment != null) {
                    parentFragment.refreshAllFragments();
                }
                break;
            case R.id.menu_topic_action_new:
                topicEditFragment = TopicEditFragment.newInstance(null, topic.getId(), TopicListFragment.class.getName());
                topicEditFragment.show(getChildFragmentManager(), TopicEditFragment.TAG);
                break;
            case R.id.menu_topic_action_edit:
                topicEditFragment = TopicEditFragment.newInstance(topic.getId(), null, TopicListFragment.class.getName());
                topicEditFragment.show(getChildFragmentManager(), TopicEditFragment.TAG);
                break;
            case R.id.menu_topic_action_delete:
                topicsManager.deleteTopic(topic.getId());
                parentFragment = (TopicsFragment) getParentFragment();
                if(parentFragment != null) {
                    parentFragment.refreshAllFragments();
                }
                break;
        }
    }

    public void refresh(List<Topic> topicList) {
        adapter.setTopicList(topicList);
        adapter.notifyDataSetChanged();
    }
}
