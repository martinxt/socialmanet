package com.martin.socialmanet.topic;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.martin.socialmanet.R;
import com.martin.socialmanet.managers.TopicsManager;
import com.martin.socialmanet.model.Topic;

import java.util.List;

public class TopicsFragment extends Fragment {
    public static final String FRAGMENT_SUBSCRIPTIONS_TAG = "fragment-subscriptions";
    public static final String FRAGMENT_UNSUBSCRIPTIONS_TAG = "fragment-unsubscriptions";

    private TextView subscriptionsTitle, topicsTitle;
    private ImageButton expandSubscriptions, expandTopics;
    private FloatingActionButton addTopicActionButton;

    private boolean subscriptionsExpanded = true;
    private boolean topicsExpanded = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.fragment_topics, container, false);

        subscriptionsTitle = layout.findViewById(R.id.section_title_subscriptions);
        topicsTitle = layout.findViewById(R.id.section_title_all_topics);
        expandSubscriptions = layout.findViewById(R.id.btn_subscriptions_expand);
        expandTopics = layout.findViewById(R.id.btn_topics_expand);
        addTopicActionButton = layout.findViewById(R.id.fab_new_topic);

        TopicsManager topicsManager = TopicsManager.getInstance(getContext());
        subscriptionsTitle.setText(getText(R.string.subscriptions) + " (" + topicsManager.getSubscriptions().size() + ")");
        topicsTitle.setText(getText(R.string.title_topics) + " (" + topicsManager.getNotSubscribedTo().size() + ")");
        expandTopics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleShowTopics();
            }
        });
        expandSubscriptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleShowSubscriptions();
            }
        });
        addTopicActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TopicEditFragment topicEditFragment = TopicEditFragment.newInstance(null, null, TopicsFragment.class.getName());
                topicEditFragment.show(getChildFragmentManager(), TopicEditFragment.TAG);
            }
        });

        if (savedInstanceState == null) {
            Bundle argsSubscriptions = new Bundle();
            argsSubscriptions.putString(TopicListFragment.TAG_FRAGMENT_TYPE, TopicListFragment.ARG_FRAGMENT_SUBSCRIPTIONS);
            //Bundle argsTopics = new Bundle();
            //argsTopics.putString(TopicListFragment.TAG_FRAGMENT_TYPE, TopicListFragment.ARG_FRAGMENT_TOPICS);
            getChildFragmentManager().beginTransaction()
                    .add(R.id.fragment_container_topics_subscription, TopicListFragment.class, argsSubscriptions, FRAGMENT_SUBSCRIPTIONS_TAG)
                    //.add(R.id.fragment_container_topics_all, TopicListFragment.class, argsTopics, FRAGMENT_UNSUBSCRIPTIONS_TAG)
                    .setReorderingAllowed(false)
                    .commit();
        }
        return layout;
    }

    private void toggleShowSubscriptions() {
        if(subscriptionsExpanded) {
            TopicListFragment fragment = (TopicListFragment) getChildFragmentManager().findFragmentByTag(FRAGMENT_SUBSCRIPTIONS_TAG);
            getChildFragmentManager().beginTransaction()
                    .remove(fragment)
                    .setReorderingAllowed(true)
                    .commit();
            expandSubscriptions.setImageDrawable(getActivity().getDrawable(R.drawable.ic_outline_arrow_drop_down_24));
        } else {
            Bundle argsSubscriptions = new Bundle();
            argsSubscriptions.putString(TopicListFragment.TAG_FRAGMENT_TYPE, TopicListFragment.ARG_FRAGMENT_SUBSCRIPTIONS);
            getChildFragmentManager().beginTransaction()
                    .add(R.id.fragment_container_topics_subscription, TopicListFragment.class, argsSubscriptions, FRAGMENT_SUBSCRIPTIONS_TAG)
                    .setReorderingAllowed(true)
                    .commit();
            expandSubscriptions.setImageDrawable(getActivity().getDrawable(R.drawable.ic_outline_arrow_drop_up_24));
        }
        subscriptionsExpanded = !subscriptionsExpanded;
    }

    private void toggleShowTopics() {
        if(topicsExpanded) {
            TopicListFragment fragment = (TopicListFragment) getChildFragmentManager().findFragmentByTag(FRAGMENT_UNSUBSCRIPTIONS_TAG);
            getChildFragmentManager().beginTransaction()
                    .remove(fragment)
                    .setReorderingAllowed(true)
                    .commit();
            expandTopics.setImageDrawable(getActivity().getDrawable(R.drawable.ic_outline_arrow_drop_down_24));
        } else {
            Bundle argsTopics = new Bundle();
            argsTopics.putString(TopicListFragment.TAG_FRAGMENT_TYPE, TopicListFragment.ARG_FRAGMENT_TOPICS);
            getChildFragmentManager().beginTransaction()
                    .add(R.id.fragment_container_topics_all, TopicListFragment.class, argsTopics, FRAGMENT_UNSUBSCRIPTIONS_TAG)
                    .setReorderingAllowed(true)
                    .commit();
            expandTopics.setImageDrawable(getActivity().getDrawable(R.drawable.ic_outline_arrow_drop_up_24));
        }
        topicsExpanded = !topicsExpanded;
    }

    public void refreshAllFragments() {
        TopicsManager topicsManager = TopicsManager.getInstance(getContext());
        List<Topic> topicList = topicsManager.getNotSubscribedTo();
        List<Topic> subscriptionsList = topicsManager.getSubscriptions();
        subscriptionsTitle.setText(getText(R.string.subscriptions) + " (" + subscriptionsList.size() + ")");
        topicsTitle.setText(getText(R.string.title_topics) + " (" + topicList.size() + ")");
        if(subscriptionsExpanded) {
            TopicListFragment fragmentSubscriptions = (TopicListFragment) getChildFragmentManager().findFragmentByTag(FRAGMENT_SUBSCRIPTIONS_TAG);
            fragmentSubscriptions.refresh(subscriptionsList);
        }
        if(topicsExpanded) {
            TopicListFragment fragmentTopics = (TopicListFragment) getChildFragmentManager().findFragmentByTag(FRAGMENT_UNSUBSCRIPTIONS_TAG);
            fragmentTopics.refresh(topicList);
        }
    }
}