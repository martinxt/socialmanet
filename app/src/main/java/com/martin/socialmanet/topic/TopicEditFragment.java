package com.martin.socialmanet.topic;

import android.os.Bundle;

import androidx.fragment.app.DialogFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.martin.socialmanet.R;
import com.martin.socialmanet.managers.TopicsManager;
import com.martin.socialmanet.model.Topic;

public class TopicEditFragment extends DialogFragment {
    public static final String TAG = "topic-edit-fragment";
    private static final String ARG_TOPIC_ID = "topic-id";
    private static final String ARG_PARENT_TOPIC_ID = "parent-topic-id";
    private static final String ARG_FRAGMENT_CALLER = "caller-fragment";

    private View rootView;
    private EditText topicCode, topicName, topicDesc;
    private Button cancelBtn, saveBtn;
    private TopicsManager topicsManager;
    private Topic topic;
    private Topic parentTopic;
    private String caller;

    public TopicEditFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * TopicEditFragment using the provided parameters.
     *
     * @param topicId Parameter 1.
     * @param parentTopicId Parameter 2.
     * @return A new instance of fragment TopicEditFragment.
     */
    public static TopicEditFragment newInstance(String topicId, String parentTopicId, String callerId) {
        TopicEditFragment fragment = new TopicEditFragment();
        Bundle args = new Bundle();
        args.putString(ARG_TOPIC_ID, topicId);
        args.putString(ARG_PARENT_TOPIC_ID, parentTopicId);
        args.putString(ARG_FRAGMENT_CALLER, callerId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        topicsManager = TopicsManager.getInstance(getContext());
        if (getArguments() != null) {
            String topicId = getArguments().getString(ARG_TOPIC_ID);
            String parentTopicId = getArguments().getString(ARG_PARENT_TOPIC_ID);
            caller = getArguments().getString(ARG_FRAGMENT_CALLER);
            if(topicId != null) {
                topic = topicsManager.getTopic(topicId);
            }
            if(parentTopicId != null) {
                parentTopic = topicsManager.getTopic(parentTopicId);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_topic_edit, container, false);
        topicCode = rootView.findViewById(R.id.topic_edit_code);
        topicName = rootView.findViewById(R.id.topic_edit_name);
        topicDesc = rootView.findViewById(R.id.topic_edit_desc);
        cancelBtn = rootView.findViewById(R.id.edit_topic_action_cancel);
        saveBtn = rootView.findViewById(R.id.edit_topic_action_save);
        if(topic != null) {
            topicCode.setText(topic.getCode());
            topicCode.setEnabled(false);
            topicName.setText(topic.getName());
            topicDesc.setText(topic.getDescription());
        }
        TopicEditFragment me = this;
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code, name, desc;
                code = topicCode.getText().toString();
                name = topicName.getText().toString();
                desc = topicDesc.getText().toString();
                if(code == null || name == null || code.trim().isEmpty() || name.trim().isEmpty()) {
                    Toast.makeText(getActivity().getApplicationContext(), R.string.topic_form_invalid, Toast.LENGTH_SHORT).show();
                    return;
                }
                if (topic != null) {
                    topic.setName(name);
                    topic.setDescription(desc);
                    topicsManager.updateTopic(topic);
                } else {
                    String topicId = parentTopic == null? code.toUpperCase() : parentTopic.getCode() + "." + code.toUpperCase();
                    Topic newTopic = new Topic(topicId, code.toUpperCase(), name, desc);
                    newTopic.setCreated(true);
                    newTopic.setCreator(true);
                    topicsManager.createTopic(newTopic);
                    if(parentTopic != null) {
                        parentTopic.setDimension(parentTopic.getDimension() + 1);
                        topicsManager.updateTopic(parentTopic);
                    }
                }
                if (caller != null && caller.equals(TopicsFragment.class.getName())) {
                    TopicsFragment parentFragment = (TopicsFragment) getParentFragment();
                    parentFragment.refreshAllFragments();
                }
                else if (caller != null && caller.equals(TopicListFragment.class.getName())) {
                    TopicListFragment parentFragment = (TopicListFragment) getParentFragment();
                    ((TopicsFragment) parentFragment.getParentFragment()).refreshAllFragments();
                }
                me.dismiss();
            }
        });
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                me.dismiss();
            }
        });
        return rootView;
    }

}