package com.martin.socialmanet.network;

import androidx.annotation.NonNull;

import com.google.android.gms.nearby.connection.Payload;
import com.google.android.gms.nearby.connection.PayloadCallback;
import com.google.android.gms.nearby.connection.PayloadTransferUpdate;
import com.martin.socialmanet.MainActivity;

public class SMPayloadCallback extends PayloadCallback {
    private MainActivity activity;

    public SMPayloadCallback(MainActivity activity) {
        this.activity = activity;
    }

    @Override
    public void onPayloadReceived(@NonNull String endpointId, @NonNull Payload payload) {
        activity.handlePayloadReceived(endpointId, payload);
    }

    @Override
    public void onPayloadTransferUpdate(@NonNull String endpointId, @NonNull PayloadTransferUpdate payloadTransferUpdate) {

    }
}
