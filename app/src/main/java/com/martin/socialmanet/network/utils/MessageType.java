package com.martin.socialmanet.network.utils;

import androidx.annotation.NonNull;

public enum MessageType {
    QUERY("QUERY"),
    PUBLICATION("PUBLICATION"),
    REFRESH_REQUEST("REFRESH-REQUEST");

    private String type;

    MessageType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type;
    }
}
