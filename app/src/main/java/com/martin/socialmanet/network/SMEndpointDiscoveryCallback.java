package com.martin.socialmanet.network;

import androidx.annotation.NonNull;

import com.google.android.gms.nearby.connection.DiscoveredEndpointInfo;
import com.google.android.gms.nearby.connection.EndpointDiscoveryCallback;
import com.martin.socialmanet.MainActivity;

public class SMEndpointDiscoveryCallback extends EndpointDiscoveryCallback {

    private MainActivity activity;

    public SMEndpointDiscoveryCallback(MainActivity activity) {
        this.activity = activity;
    }

    @Override
    public void onEndpointFound(@NonNull String endpointId, @NonNull DiscoveredEndpointInfo discoveredEndpointInfo) {
        activity.handleEndpointFound(endpointId);
    }

    @Override
    public void onEndpointLost(@NonNull String endpointId) {
        activity.handleEndpointLost(endpointId);
    }
}
