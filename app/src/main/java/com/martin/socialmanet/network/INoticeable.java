package com.martin.socialmanet.network;

@FunctionalInterface
public interface INoticeable {
    void onNetworkStatusChange(boolean isOnline);
}
