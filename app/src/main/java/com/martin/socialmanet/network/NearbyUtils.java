package com.martin.socialmanet.network;

import android.content.Context;

import androidx.annotation.NonNull;

import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.connection.ConnectionLifecycleCallback;
import com.google.android.gms.nearby.connection.EndpointDiscoveryCallback;
import com.google.android.gms.nearby.connection.Payload;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.martin.socialmanet.MainActivity;
import com.martin.socialmanet.network.utils.MessageType;

import java.io.UnsupportedEncodingException;
import java.util.List;

public class NearbyUtils {
    private MainActivity activity;

    public NearbyUtils(MainActivity activity) {
        this.activity = activity;
    }

    public void connect(final String endpointId, ConnectionLifecycleCallback connectionLifecycleCallback) {
        String UserID = NearbyUtils.getUserNickName(activity.getApplicationContext());
        Nearby.getConnectionsClient(activity)
                .requestConnection(UserID, endpointId, connectionLifecycleCallback)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        activity.handleRequestConnectionStatus(endpointId,true);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        activity.handleRequestConnectionStatus(endpointId, false);
                    }
                });
    }

    public void startAdvertising(ConnectionLifecycleCallback callback) {
        String UserID = getUserNickName(activity.getApplicationContext());
        Nearby.getConnectionsClient(activity)
            .startAdvertising(
                    UserID, NearbyConfig.SERVICE_ID, callback, NearbyConfig.getAdvertisingOptions())
            .addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    activity.handleAdvertisingStatus(true);
                }
            })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    activity.handleAdvertisingStatus(false);
                }
            });
    }

    public void startDiscovery(EndpointDiscoveryCallback callback) {
        Nearby.getConnectionsClient(activity)
            .startDiscovery(NearbyConfig.SERVICE_ID, callback, NearbyConfig.getDiscoveryOptions())
            .addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    activity.handleDiscoveringStatus(true);
                }
            })
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    activity.handleDiscoveringStatus(false);
                }
            });
    }

    public void stopAdvertising() {
        Nearby.getConnectionsClient(activity).stopAdvertising();
    }

    public void stopDiscovery() {
        Nearby.getConnectionsClient(activity).stopDiscovery();
    }

    public Payload createPayload(final String message, final MessageType type) {
        String msg = type.toString() + message;
        try {
            return Payload.fromBytes(msg.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            //ignore
        }
        return Payload.fromBytes(msg.getBytes());
    }

    public void sendPayload(final String endpointId, final Payload payload) {
        Nearby.getConnectionsClient(activity)
            .sendPayload(endpointId, payload)
            .addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    //retry
                    Nearby.getConnectionsClient(activity).sendPayload(endpointId, payload);
                }
            });
    }

    public void sendPayload(final List<String> endpointIds, final Payload payload) {
        Nearby.getConnectionsClient(activity)
                .sendPayload(endpointIds, payload)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        //retry
                        Nearby.getConnectionsClient(activity).sendPayload(endpointIds, payload);
                    }
                });
    }

    public static String getUserNickName(Context ctx) {
        return "" + Nearby.getConnectionsClient(ctx).getInstanceId();
    }
}
