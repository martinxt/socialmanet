package com.martin.socialmanet.network;

import com.google.android.gms.nearby.connection.AdvertisingOptions;
import com.google.android.gms.nearby.connection.DiscoveryOptions;
import com.google.android.gms.nearby.connection.Strategy;

public class NearbyConfig {
    public static String SERVICE_ID = "com.martin.socialmanet";

    public static AdvertisingOptions getAdvertisingOptions() {
        return new AdvertisingOptions.Builder().setStrategy(Strategy.P2P_CLUSTER).build();
    }

    public static DiscoveryOptions getDiscoveryOptions() {
        return new DiscoveryOptions.Builder().setStrategy(Strategy.P2P_CLUSTER).build();
    }
}
