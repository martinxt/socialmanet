package com.martin.socialmanet.network.utils;

public enum ConnectionStatus {
    FOUND, LOST, CONNECTED, DISCONNECTED, REJECTED, ERROR, INVITED
}
