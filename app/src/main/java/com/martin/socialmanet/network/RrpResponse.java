package com.martin.socialmanet.network;

import com.martin.socialmanet.model.Publication;

import java.util.List;

public class RrpResponse {
    private List<Publication> publications;
    private List<String> pubRequestIds;

    public RrpResponse(){  }
    public RrpResponse(List<Publication> publications, List<String> pubRequestIds) {
        this.publications = publications;
        this.pubRequestIds = pubRequestIds;
    }

    public List<Publication> getPublications() {
        return publications;
    }

    public void setPublications(List<Publication> publications) {
        this.publications = publications;
    }

    public List<String> getPubRequestIds() {
        return pubRequestIds;
    }

    public void setPubRequestIds(List<String> pubRequestIds) {
        this.pubRequestIds = pubRequestIds;
    }
}

