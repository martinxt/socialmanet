package com.martin.socialmanet.network;

import com.martin.socialmanet.model.Publication;
import com.martin.socialmanet.utils.Query;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface RrpService {
    @POST("/rrp-api/v1/publications/get")
    Call<RrpResponse> getMissingPublications(@Body Query query);

    @POST("/rrp-api/v1/publications/save")
    Call<List<Publication>> savePublications(@Body List<Publication> publications);

}
