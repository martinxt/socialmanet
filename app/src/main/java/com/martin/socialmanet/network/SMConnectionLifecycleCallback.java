package com.martin.socialmanet.network;

import androidx.annotation.NonNull;

import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.connection.ConnectionInfo;
import com.google.android.gms.nearby.connection.ConnectionLifecycleCallback;
import com.google.android.gms.nearby.connection.ConnectionResolution;
import com.google.android.gms.nearby.connection.ConnectionsStatusCodes;
import com.google.android.gms.nearby.connection.PayloadCallback;
import com.martin.socialmanet.MainActivity;
import com.martin.socialmanet.network.utils.ConnectionStatus;

public class SMConnectionLifecycleCallback extends ConnectionLifecycleCallback {
    private MainActivity activity;
    private PayloadCallback payloadCallback;

    public SMConnectionLifecycleCallback(MainActivity activity, PayloadCallback payloadCallback) {
        this.activity = activity;
        this.payloadCallback = payloadCallback;
    }

    @Override
    public void onConnectionInitiated(@NonNull String endpointId, @NonNull ConnectionInfo connectionInfo) {
        Nearby.getConnectionsClient(activity).acceptConnection(endpointId, payloadCallback);
    }

    @Override
    public void onConnectionResult(@NonNull String endpointId, @NonNull ConnectionResolution connectionResolution) {
        switch (connectionResolution.getStatus().getStatusCode()) {
            case ConnectionsStatusCodes.STATUS_OK:
                // We're connected! Can now start sending and receiving data.
                activity.handleConnectionStatus(endpointId, ConnectionStatus.CONNECTED);
                break;
            case ConnectionsStatusCodes.STATUS_CONNECTION_REJECTED:
                // The connection was rejected by one or both sides.
                activity.handleConnectionStatus(endpointId, ConnectionStatus.REJECTED);
                break;// The connection broke before it was able to be accepted.
            default:
                // Unknown status code
                activity.handleConnectionStatus(endpointId, ConnectionStatus.ERROR);
        }
    }

    @Override
    public void onDisconnected(@NonNull String endpointId) {
        activity.handleConnectionStatus(endpointId, ConnectionStatus.DISCONNECTED);
    }
}
