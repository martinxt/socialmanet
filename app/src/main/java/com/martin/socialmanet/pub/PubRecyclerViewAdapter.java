package com.martin.socialmanet.pub;

import android.graphics.drawable.Drawable;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.martin.socialmanet.R;
import com.martin.socialmanet.model.Publication;

import java.util.List;

public class PubRecyclerViewAdapter extends RecyclerView.Adapter<PubRecyclerViewAdapter.ViewHolder> {

    private List<Publication> publicationList;
    private final PubFragment mListener;

    public PubRecyclerViewAdapter(List<Publication> publications, PubFragment listener) {
        publicationList = publications;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_pub, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Publication publication = publicationList.get(position);
        holder.mItem = publication;
        String topic = publication.getTopic() == null? publication.getTopicId() : publication.getTopic().getName();
        holder.mPublicationTopicName.setText(topic);
        holder.mPublicationDescription.setText(publication.getDescription());
        if(publication.getPath() == null) {
            holder.mPublicationAttachment.setVisibility(View.INVISIBLE);
        }
        holder.mPublicationAvatar.setImageDrawable(getPublicationIcon(publication));
    }

    @Override
    public int getItemCount() {
        return publicationList.size();
    }

    public void setPublicationList(List<Publication> pubs) {
        this.publicationList = pubs;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements
            View.OnCreateContextMenuListener, MenuItem.OnMenuItemClickListener{
        public final View mView;
        public final TextView mPublicationTopicName;
        public final TextView mPublicationDescription;
        public final ImageView mPublicationAvatar;
        public final ImageView mPublicationAttachment;
        public Publication mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mPublicationTopicName = view.findViewById(R.id.it_pub_topic_name);
            mPublicationDescription = view.findViewById(R.id.it_pub_desc);
            mPublicationAvatar = view.findViewById(R.id.it_pub_avatar);
            mPublicationAttachment = view.findViewById(R.id.it_pub_attachment);
            view.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            menu.add(Menu.NONE, R.id.menu_pub_action_delete, Menu.NONE, R.string.delete)
                    .setOnMenuItemClickListener(this);
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            mListener.onMenuItemSelected(item, mItem);
            return false;
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mPublicationDescription.getText() + "'";
        }
    }

    private Drawable getPublicationIcon(Publication publication) {
        if (publication.getPath() == null) {
            return mListener.getContext().getDrawable(R.drawable.ic_baseline_short_text_24);
        }
        return mListener.getContext().getDrawable(R.drawable.ic_outline_unknown_file_24);
    }
}
