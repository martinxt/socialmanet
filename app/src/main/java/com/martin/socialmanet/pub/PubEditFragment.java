package com.martin.socialmanet.pub;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;

import com.martin.socialmanet.R;
import com.martin.socialmanet.managers.PublicationManager;
import com.martin.socialmanet.managers.TopicsManager;
import com.martin.socialmanet.model.Publication;
import com.martin.socialmanet.model.Topic;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class PubEditFragment extends DialogFragment implements AdapterView.OnItemSelectedListener {

    public static final String TAG = "publication-edit-fragment";
    private static final String ARG_PUB_ID = "publication-id";

    private View rootView;
    private EditText description;
    private Button cancelBtn, saveBtn;
    private Spinner topicListSpinner;
    private Publication publication;
    private TopicsManager topicsManager;
    private PublicationManager publicationManager;
    private List<Topic> topicList;

    public PubEditFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param pubId The publication ID for read only mode.
     * @return A new instance of fragment PubEditFragment.
     */
    public static PubEditFragment newInstance(String pubId) {
        PubEditFragment fragment = new PubEditFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PUB_ID, pubId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        topicsManager = TopicsManager.getInstance(getContext());
        topicList = topicsManager.getSubscriptions();
        publicationManager = PublicationManager.getInstance(getContext());
        if (getArguments() != null) {
            String pubId = getArguments().getString(ARG_PUB_ID);
            if(pubId != null) {
                publication = publicationManager.getPublication(pubId);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_pub_edit, container, false);
        topicListSpinner = rootView.findViewById(R.id.pub_edit_topics_list);
        topicListSpinner.setAdapter(new ArrayAdapter<String>(
                getContext(),
                android.R.layout.simple_spinner_item,
                topicList.stream().map(t -> t.getName()).collect(Collectors.toList())));
        topicListSpinner.setOnItemSelectedListener(this);
        description = rootView.findViewById(R.id.text_pub_edit_description);
        cancelBtn = rootView.findViewById(R.id.button_pub_edit_action_cancel);
        saveBtn = rootView.findViewById(R.id.button_pub_edit_action_save);

        PubEditFragment me = this;
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(publication == null) {
                    Toast.makeText(getActivity().getApplicationContext(), R.string.choose_a_topic, Toast.LENGTH_SHORT).show();
                    return;
                }
                if(description.getText() == null) {
                    Toast.makeText(getActivity().getApplicationContext(), R.string.description_mandatory, Toast.LENGTH_SHORT).show();
                    return;
                }
                Publication currentPub = getCurrentPub(Optional.empty());
                currentPub.setDescription((description.getText().toString()));
                publicationManager.savePublication(currentPub);

                PubFragment parentFragment = (PubFragment) getParentFragment();
                parentFragment.notifyNewPublicationCreated(currentPub);
                parentFragment.refresh();
                me.dismiss();
            }
        });
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                me.dismiss();
            }
        });
        return rootView;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Topic selectedTopic = topicList.get(position);
        Publication currentPub = getCurrentPub(Optional.of(selectedTopic));
        if(currentPub.getTopicId() == null) {
            currentPub.setPublishDate(new Date().getTime());
            currentPub.setValidity(currentPub.getPublishDate() + Publication.DEFAULT_PUBLICATION_VALIDITY);
        }
        if(!selectedTopic.getId().equals(currentPub.getTopicId())) {
            currentPub.setTopic(selectedTopic);
            currentPub.setTopicId(selectedTopic.getId());
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        //Nothing to do
    }

    private Publication getCurrentPub(Optional<Topic> topic) {
        if(publication == null) {
            if(topic.isPresent()) {
                publication = new Publication(topic.get());
            } else {
                publication = new Publication();
            }
        }
        return publication;
    }
}