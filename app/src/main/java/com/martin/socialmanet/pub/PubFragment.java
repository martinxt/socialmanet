package com.martin.socialmanet.pub;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.martin.socialmanet.MainActivity;
import com.martin.socialmanet.R;
import com.martin.socialmanet.managers.PublicationManager;
import com.martin.socialmanet.managers.TopicsManager;
import com.martin.socialmanet.model.Publication;
import com.martin.socialmanet.model.Topic;
import com.martin.socialmanet.topic.TopicEditFragment;
import com.martin.socialmanet.topic.TopicListFragment;
import com.martin.socialmanet.topic.TopicsFragment;

import java.util.List;

public class PubFragment extends Fragment {

    private PubRecyclerViewAdapter adapter;
    private FloatingActionButton addPubActionButton;
    PublicationManager publicationManager;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public PubFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.fragment_pub_list, container, false);

        Context context = layout.getContext();
        RecyclerView recyclerView = layout.findViewById(R.id.pubs_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        publicationManager = PublicationManager.getInstance(getContext());
        List<Publication> publicationList = publicationManager.getPublications();

        adapter = new PubRecyclerViewAdapter(publicationList, this);
        recyclerView.setAdapter(adapter);

        addPubActionButton = layout.findViewById(R.id.fab_new_pub);
        addPubActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PubEditFragment pubEditFragment = PubEditFragment.newInstance(null);
                pubEditFragment.show(getChildFragmentManager(), PubEditFragment.TAG);
            }
        });

        return layout;
    }

    public void onMenuItemSelected(MenuItem item, Publication pub) {
        switch (item.getItemId()) {
            case R.id.menu_pub_action_delete:
                publicationManager.deletePublication(pub.getPublicationId());
                this.refresh();
                break;
        }
    }

    public void refresh() {
        adapter.setPublicationList(publicationManager.getPublications());
        adapter.notifyDataSetChanged();
    }

    public void notifyNewPublicationCreated(Publication pub) {
        MainActivity activity = (MainActivity) getActivity();
        activity.handleNewPublicationCreated(pub);
    }

}
